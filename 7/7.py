import sys
import math
import statistics as s

def fuel(n):
    return n*(n+1)/2

inputs = open(sys.argv[1]).read().split(",")
inputs = [int(x) for x in inputs]
target = s.median(inputs)
a = sum([abs(target-x) for x in inputs])
print(f"answer 1 is {a}\n target was {target}")
target = math.floor(s.mean(inputs))
a = sum([fuel(abs(target-x)) for x in inputs])
print(f"answer 2 is {a}\n target was {target}")
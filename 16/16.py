import sys
from operator import mul
from functools import reduce
from enum import Enum, auto
filename = sys.argv[1] if len(sys.argv) > 1 else None

def hex_to_bits(h):
	n = int(h, 16) 
	bits = []
	while n > 0:
		bits.insert(0, n % 2)
		n = n >> 1
	pad = [0 for _ in range(4-len(bits))]
	return pad + bits

def hexs_to_bits(hs):
	bits = []
	for h in hs:
		bits += hex_to_bits(h)
	return bits

CHUNK_SIZE = 5

def to_str(bits):
	return "".join(map(str,bits))

def to_number(bits):
	return int(to_str(bits),2)

def value_from_bits(bits):
	i = 0
	d = []
	more_chunks = True
	while more_chunks:
		chunk = bits[i+1:i+CHUNK_SIZE]
		d += chunk
		more_chunks = bits[i]
		i += CHUNK_SIZE
	data_length = i
	return data_length, to_number(d)

class Packet():
	def __init__(self, bits):
		#print(f"parsing packet with bits {to_str(bits)}")
		self.bits = bits
		self.version = to_number(self.bits[:3])
		self.type = to_number(self.bits[3:6])
		self.subpackets = []
		if self.type == 4:
			data_length, self.value = value_from_bits(bits[6:])
			self.packet_length = 6 + data_length
		else:
			length_type_id = bits[6]
			if length_type_id:
				l_size = 11
				num_subpackets = to_number(bits[7:7+l_size])
				subpacket_start = 7+l_size
				i = subpacket_start
				for _ in range(num_subpackets):
					p = Packet(bits[i:])
					i += p.packet_length
					self.subpackets.append(p)
			else:
				l_size = 15
				subpackets_len = to_number(bits[7:7+l_size])
				subpacket_start = 7+l_size
				i = subpacket_start
				subpacket_end = subpacket_start + subpackets_len
				while i < subpacket_end:
					p = Packet(bits[i:subpacket_end])
					i += p.packet_length
					self.subpackets.append(p)
			self.packet_length = 7 + l_size + sum([s.packet_length for s in self.subpackets])
			
def version_sum(packet):
	return packet.version + sum([version_sum(s) for s in packet.subpackets])

class PacketType(Enum):
	ADDITION = 0
	MULTIPLICATION = 1
	MIN = 2
	MAX = 3
	NUMBER = 4
	GREATER = 5
	LESSER = 6
	EQUALS = 7

def calculate(packet):
	values = [calculate(s) for s in packet.subpackets]
	if PacketType(packet.type) == PacketType.ADDITION:
		return sum(values)
	if PacketType(packet.type) == PacketType.MULTIPLICATION:
		return reduce(mul, values, 1)
	if PacketType(packet.type) == PacketType.MIN:
		return min(values)
	if PacketType(packet.type) == PacketType.MAX:
		return max(values)
	if PacketType(packet.type) == PacketType.NUMBER:
		return packet.value
	if PacketType(packet.type) == PacketType.GREATER:
		return int(values[0] > values[1])
	if PacketType(packet.type) == PacketType.LESSER:
		return int(values[0] < values[1])
	if PacketType(packet.type) == PacketType.EQUALS:
		return int(values[0] == values[1])

if not filename:		
	p = Packet(hexs_to_bits("D2FE28"))
	assert p.version == 6
	assert p.value == 2021
	assert p.packet_length == 21

	p = Packet(hexs_to_bits("38006F45291200"))
	assert len(p.subpackets) == 2
	assert p.subpackets[0].value == 10
	assert p.subpackets[1].value == 20

	p = Packet(hexs_to_bits("EE00D40C823060"))
	assert len(p.subpackets) == 3
	for i in range(3):
		assert p.subpackets[i].value == i + 1

	assert version_sum(Packet(hexs_to_bits("8A004A801A8002F478"))) == 16
	assert version_sum(Packet(hexs_to_bits("620080001611562C8802118E34"))) == 12
	assert version_sum(Packet(hexs_to_bits("C0015000016115A2E0802F182340"))) == 23
	assert version_sum(Packet(hexs_to_bits("A0016C880162017C3686B18A3D4780"))) == 31

	assert calculate(Packet(hexs_to_bits("C200B40A82"))) == 3
	assert calculate(Packet(hexs_to_bits("04005AC33890"))) == 54
	assert calculate(Packet(hexs_to_bits("880086C3E88112"))) == 7
	assert calculate(Packet(hexs_to_bits("CE00C43D881120"))) == 9
	assert calculate(Packet(hexs_to_bits("D8005AC2A8F0"))) == 1
	assert calculate(Packet(hexs_to_bits("F600BC2D8F"))) == 0
	assert calculate(Packet(hexs_to_bits("9C005AC2F8F0"))) == 0
	assert calculate(Packet(hexs_to_bits("9C0141080250320F1802104A08"))) == 1
	print("all tests succesful")

else:
	print(f"opening {filename}")
	input = open(filename).read()
	print(f"answer 1 = {version_sum(Packet(hexs_to_bits(input)))}")
	print(f"answer 2 = {calculate(Packet(hexs_to_bits(input)))}")
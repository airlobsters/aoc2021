import sys
filename = sys.argv[1] if len(sys.argv) > 1 else None
def parse_input(s):
	s = s.split(":")[1].strip()
	xs, ys = s.split(",")
	xstart, xend = map(int, xs.strip("xy= ").split(".."))
	ystart, yend = map(int, ys.strip("xy= ").split(".."))
	return (xstart, xend, ystart, yend)

def add_tup(t1, t2):
	return tuple(map(sum, zip(t1, t2)))

def sign(n):
	return (n > 0) - (n < 0)

def drag(v):
	return (v[0] - sign(v[0]), v[1])

class Probe():
	def __init__(self, velocity, pos=(0,0)) -> None:
		self.pos = pos
		self.v = velocity
	
	def step(self):
		self.pos = add_tup(self.v, self.pos)
		self.v = drag(self.v)
		self.v = (self.v[0], self.v[1] - 1)
	
	def is_in_target(self, target):
		xstart, xend, ystart, yend = target
		x, y = self.pos
		return xstart <= x <= xend and ystart <= y <= yend
	
	def is_past_target(self, target):
		return (self.is_past_target_x(target) or 
				self.is_past_target_y(target))

	def is_past_target_x(self, target):
		xstart, xend, ystart, yend = target
		x, y = self.pos
		return  x > max(xend, xstart)
	
	def is_past_target_y(self, target):
		xstart, xend, ystart, yend = target
		x, y = self.pos
		return  y < min(yend, ystart)

import matplotlib.pyplot as plt
def print_dots(dots):
	plt.scatter(*zip(*dots))
	plt.savefig("out.png")

if filename:
	input = open(filename).read()
	target = parse_input(input)
	total_max_y = 0
	hits = []
	for try_x in range(max(target)+1):
		for try_y in range(min(target)-1, max(target)+1, 1):
			p = Probe((try_x, try_y))
			max_y = 0
			while not p.is_past_target(target):
				p.step()
				max_y = p.pos[1] if p.pos[1] > max_y else max_y
				if p.is_in_target(target):
					total_max_y = max_y if max_y > total_max_y else total_max_y
					hits.append((try_x, try_y))
	unique_hits = list(set(hits))
	print(f"answer 1 = {total_max_y}")
	print(f"answer 2 = {len(unique_hits)}")
	print_dots(unique_hits)
	
else:
	# tests
	assert add_tup((0,0), (1,1)) == (1,1)
	assert add_tup((0,0), (0,0)) == (0,0)
	assert add_tup((0,-1), (1,0)) == (1,-1)
	assert drag((-1,-1)) == (0,-1)
	assert drag((1,1)) == (0,1)
	assert drag((0,0)) == (0,0)

	assert Probe((0,0), (7,2)).is_in_target((1,10,1,10))
	# x smaller, y smaller
	assert Probe((0,0), (-1,-2)).is_past_target((1,10,1,10))
	# x larger, y smaller
	assert Probe((0,0), (11,-2)).is_past_target((1,10,1,10))
	# x smaller, y larger
	assert Probe((0,0), (-10,-11)).is_past_target((1,10,1,10))
	# x larger and y larger
	assert Probe((0,0), (11,11)).is_past_target((1,10,1,10))
	print("all tests succesful")


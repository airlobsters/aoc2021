import sys
filename = sys.argv[1] if len(sys.argv) > 1 else "17/input_test.txt"
def parse_input(s):
	s = s.split(":")[1].strip()
	xs, ys = s.split(",")
	xstart, xend = map(int, xs.strip("xy= ").split(".."))
	ystart, yend = map(int, ys.strip("xy= ").split(".."))
	return (xstart, xend, ystart, yend)

input = open(filename).read()
target = parse_input(input)
hits = []

MAX_STEPS = 500
for steps in range(1, MAX_STEPS):
    for tx in range(target[0],target[1]+1):
        vx = (((steps-1)/2)*steps + tx)/steps
        # if tx is reachable in steps
        if vx.is_integer() and vx >= steps:
            for ty in range(target[2],target[3]+1):
                vy = (((steps-1)/2)*steps + ty)/steps
                if vy.is_integer():
                    hits.append((int(vx),int(vy)))
            #if x speed is 0 over the target then all subsequent steps are also possible
            if vx == steps:
                for sub_steps in range(steps,MAX_STEPS):
                    for ty in range(target[2],target[3]+1):
                        vy = (((sub_steps-1)/2)*sub_steps + ty)/sub_steps
                        if vy.is_integer():
                            hits.append((int(vx),int(vy)))
hits = list(set(hits))
# velocity_lines = open("initial_velocities.txt").read().split("\n")
# velocities = [line.split() for line in velocity_lines]
# velocities = [i for sub in velocities for i in sub]
# velocities = list(map(lambda x: x.split(","), velocities))
# velocities = list(map(lambda x: (int(x[0]), int(x[1])), velocities))
# print(f"velocities not in hits: {[v for v in velocities if v not in hits]}")
# print(f"hits not in velocities: {[v for v in hits if v not in velocities]}")
# velocities = list(set(velocities))

n = max([x[1] for x in hits])
print(f"answer 1 = {(n*(n + 1))/2}")
print(f"answer 2 = {len(hits)}")
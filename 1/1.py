#challenge 1
l = [int(x) for x in open("input.txt").read().strip().split("\n")]

def num_depth_increases(l):
    return sum([b > a for (a,b) in zip(l, l[1:])])

assert num_depth_increases([1]) == 0
assert num_depth_increases([1,2]) == 1
assert num_depth_increases([1,2,3]) == 2
assert num_depth_increases([1,2,2,2,2,3]) == 2
assert num_depth_increases([1,2,-1,2,2,3]) == 3
assert num_depth_increases([0,1,0,1,0,1]) == 3
assert num_depth_increases([199,200,208,210,200,207,240,269,260,263]) == 7

print(num_depth_increases(l))

# challenge 2
def zip_window(l1, l2, size):
    return [(l1[i:i+size], l2[i:i+size]) for i in range(min(len(l1), len(l2)) - size + 1)]

assert zip_window([1,2,3,4], [5,6,7,8], 3) == [
    ([1,2,3], [5,6,7]), 
    ([2,3,4], [6,7,8])
]
assert zip_window([1,2,3,4], [2,3,4], 3) == [
    ([1,2,3], [2,3,4])
]

def num_sum_depth_increases(l, window_size):
    return sum([sum(b) > sum(a) for (a, b) in zip_window(l, l[1:], window_size)])

print(num_sum_depth_increases(l, 3))
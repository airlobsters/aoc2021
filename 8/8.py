import sys
lines = open(sys.argv[1]).read().strip().split("\n")

def sortstr(s):
	l = [c for c in s]
	l.sort()
	return "".join(l)

def parse_line(l):
	patterns, output_values = l.split("|")
	patterns = list(map(sortstr, patterns.strip().split(" ")))
	output_values = list(map(sortstr, output_values.strip().split(" ")))
	return patterns, output_values

inputs = [parse_line(l) for l in lines]

rules = {
	1: {
		"len": 2,
		"conjunct": [],
	},
	7: {
		"len": 3,
		"conjunct": [],
	},
	3: {
		"len" : 5,
		"conjunct": [(7, 3)]
	},
	0: {
		"len" : 6,
		"conjunct": [(7, 3), (3, 4)]
	},
	4: {
		"len": 4,
		"conjunct": [],
	},
	2: {
		"len" : 5,
		"conjunct": [(4, 2)]
	},
	5: {
		"len" : 5,
		"conjunct": [(4, 3)]
	},
	6: {
		"len" : 6,
		"conjunct": [(7, 2)]
	},
	
	8: {
		"len": 7,
		"conjunct": [],
	},
	9: {
		"len" : 6,
		"conjunct": [(7, 3), (3, 5)]
	},
}

def str_conjunct(s1, s2):
	return "".join([s for s in s1 if s in s2])

def rule_matches(rule, pattern, mapping):
	if rule["len"] == len(pattern):
		for con, amount in rule["conjunct"]:
			if len(str_conjunct(mapping[con], pattern)) != amount:
				return False
		return True
	return False

num = 0
for patterns, out_values in inputs:
	mapping = {}
	for rule in [rules[i] for i in [1,4,7,8]]:
		for o in out_values:
			if rule_matches(rule, o, mapping):
				num += 1
print(f"answer 1 = {num}")

num = 0
for patterns, out_values in inputs:
	mapping = {}
	for r in rules:
		for p in [p for p in patterns if not p in mapping]:
			if rule_matches(rules[r], p, mapping):
				mapping[p] = r
				mapping[r] = p
	answer = int("".join([str(mapping[val]) for val in out_values]))
	num += answer

print(f"answer 2 = {num}")
import sys
from functools import reduce
WINDOW_SIZE = 3

filename = sys.argv[1] if len(sys.argv) > 1 else "input_test.txt"
def read_input(filename):
    lines = open(filename).read().split("\n")
    algo = [c == "#" for c in lines[0]]
    image_in = [[c == "#" for c in line] for line in lines[2:]]
    return algo, image_in

def extend_image(image, expanse):
    expand_amount = WINDOW_SIZE - 1
    w = len(image[0])
    pad_h = [expanse for _ in range(expand_amount)]
    pad_v = [[expanse for _ in range(w + expand_amount * 2)] for _ in range(expand_amount)]
    result = (
        pad_v +
        [pad_h + row + pad_h for row in image] +
        pad_v
    )
    return result

def update_expanse(expanse, algo):
    if not expanse:
        return algo[0]
    else:
        return algo[len(algo) -1]

def print_image(image, expanse):
    print("".join(["_" for _ in range(len(image[0])+ 2*WINDOW_SIZE)]))
    w = len(image[0]) + 2*WINDOW_SIZE
    pad = "#" if expanse else "."
    for _ in range(WINDOW_SIZE):
        for _ in range(w):
            print(pad, end="")
        print()
    
    for row in image:
        for _ in range(WINDOW_SIZE):
            print(pad, end="")
        for col in row:
            print("#" if col else ".", end="")
        for _ in range(WINDOW_SIZE):
            print(pad, end="")
        print()
    for _ in range(WINDOW_SIZE):
        for _ in range(w):
            print(pad, end="")
        print()

def window_to_number(window):
    return reduce(lambda a,b: (a << 1) | b, window, 0)

def process_image(image, algo):
    W = WINDOW_SIZE
    out = []
    for r in range(len(image) - W + 1):
        outrow = []
        for c in range(len(image[0]) - W + 1):
            window = [image[r+wr][c+wc]
                    for wr in range(W)
                    for wc in range(W)]
            outrow += [algo[window_to_number(window)]]
        out += [outrow]
    return out

def lit_pixels(image):
    return sum([sum(r) for r in image])

algo, image = read_input(filename)
steps = int(sys.argv[2]) if len(sys.argv) > 2 else 2
# the image expands to infinity in all directions with this value
expanse = False
print_image(image, expanse)
for step in range(steps):
    image= extend_image(image, expanse)
    print_image(image, expanse)
    image = process_image(image, algo)
    expanse = update_expanse(expanse, algo)
    print_image(image, expanse)

print(lit_pixels(image))
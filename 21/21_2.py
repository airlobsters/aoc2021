score_limit = 21
import copy

class State():
    def __init__(self, positions, scores, active_player) -> None:
        self.positions = positions
        self.scores = scores
        self.active_player = active_player
    def update(self, roll):
        self.positions[self.active_player] = ((self.positions[self.active_player] + roll) - 1) % 10 + 1 
        self.scores[self.active_player] += self.positions[self.active_player]
        self.active_player = (self.active_player + 1) % 2

def wadd(l):
    result = [0,0]
    for e in l:
        result = [result[0]+e[0],result[1]+e[1]]
    return result

possible_outcomes = {6: 7, 5: 6, 7: 6, 4: 3, 8: 3, 3: 1, 9: 1}

def count_wins(s, u):
    # if non active player has won
    if s.scores[(s.active_player + 1) % 2] >= score_limit:
        return [u, 0] if s.active_player else [0, u]
    newstates = []
    for p in possible_outcomes:
        newstate = copy.deepcopy(s)
        newstate.update(p)
        newstates.append((newstate, possible_outcomes[p]))
    wins = wadd([count_wins(n[0], u*n[1]) for n in newstates])
    return wins

print(count_wins(State([4,8], [0,0], 0), 1))
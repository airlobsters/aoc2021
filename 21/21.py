positions = [6,9]
# positions = [4,8]
num_players = 2
num_rolls = 3
class DeterministicDie():
    def __init__(self) -> None:
        self.value = 1
        self.rolled = 0
    def roll(self):
        self.rolled += 1
        result = self.value
        self.value = (self.value ) % 100 + 1
        return result

class State():
    def __init__(self, positions, scores, active_player) -> None:
        self.positions = positions
        self.scores = scores
        self.active_player = active_player
    def update(self, roll):
        self.positions[self.active_player] = ((self.positions[self.active_player] + roll) - 1) % 10 + 1 
        self.scores[self.active_player] += self.positions[self.active_player]
        self.active_player = (self.active_player + 1) % 2

game_ended = False
d = DeterministicDie()
s = State([6,9],[0,0], 0)
rounds_played = 0
while not game_ended:
    rounds_played += 1
    move = sum([d.roll() for _ in range(num_rolls)])
    s.update(move)
    if max(s.scores) >= 1000:
        game_ended = True

print(min(s.scores) * d.rolled)
import sys
lines = open(sys.argv[1]).read().strip().split("\n")
inputs = [[int(x) for x in l] for l in lines]
octos = []

flashes = 0
simul_flashes = 0

def is_adjacent(c1, c2):
		if c1[0] == c2[0] and c1[1] == c2[1]:
			return False
		if abs(c1[0] - c2[0]) <= 1 and abs(c1[1] - c2[1]) <= 1:
			return True
		return False
		
class Octo():
	def __init__(self, energy, coords):
		self.energy = energy
		self.coords = coords
		self.adjacent_octos = []
		self.flashed = False
	def __str__(self):
		return str(self.energy)
	def flash(self):
		global flashes
		global simul_flashes
		flashes += 1
		simul_flashes += 1
		self.flashed = True
		self.energy = 0
		for o in self.adjacent_octos:
			if not o.flashed:
				o.energy += 1
				if o.energy > 9:
					o.flash()
		
for r, row in enumerate(inputs):
	for c, col in enumerate(row):
		o = Octo(col, (r, c))
		octos.append(o)

for octo1 in octos:
	for octo2 in octos:
		if is_adjacent(octo1.coords, octo2.coords):
			octo1.adjacent_octos.append(octo2)

def print_octos(step):
	print(f"step{step+1}", end = "")
	for o in octos:
		if o.coords[1] == 0:
			print("")
		print(o, end="")
	print("\n")
						
for step in range(400):
	simul_flashes = 0
	for o in octos:
		o.energy += 1
	for o in octos:
		if o.energy > 9:
			o.flash()
	for o in octos:
		o.flashed = False
	if simul_flashes == len(octos):
		print(f"all flash simultaniously at step {step+1}")

print_octos(step)

print(flashes)
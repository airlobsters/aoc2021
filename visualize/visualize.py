import json
import sys
j= json.load(open(sys.argv[1]))
FILENAME = "out.svg"
members = [j['members'][a] for a in j['members']]
num_members = len(members)
records = []
star_counts = [[0 for i in range(3)] for i in range(26)]
for m in members:
	cdl = m["completion_day_level"]
	for day in cdl:
		for star in cdl[day]:
			records.append({
				"name": m['name'],
				"day": int(day),
				"star": int(star),
				"ts": cdl[day][star]['get_star_ts']
			})
			
records.sort(key = lambda x: x['ts'])

name_scores = {}
gold_stars = []
for m in members:
	name_scores[m['name']] = 0
scores = []
for r in records:
	name_scores[r['name']] += num_members - star_counts[r['day']][r['star']]
	scores.append({
		"name": r['name'],
		"ts": r['ts'],
		"score": name_scores[r['name']]
	})
	star_counts[r['day']][r['star']] += 1
	if star_counts[r['day']][r['star']] == 1:
		gold_stars.append(scores[-1])

import matplotlib.pyplot as plt
import datetime

#sort name scores
name_scores = {k: v for k, v in sorted(name_scores.items(), key=lambda item: item[1], reverse=True)}

for name in name_scores:
	x = [datetime.datetime.fromtimestamp(r['ts']) for r in scores if r['name'] == name]
	y=  [r['score'] for r in scores if r['name'] == name]
	plt.plot(x, y, label=name)

x = [datetime.datetime.fromtimestamp(r['ts']) for r in gold_stars]
y=  [r['score'] for r in gold_stars]
plt.plot(x, y, "y*", label='_nolegend_')

plt.title("Advent of Code scores 2021")
plt.xticks(rotation=20, ha='right')
plt.legend(loc="upper left")
plt.ylabel("score")
plt.savefig(FILENAME)
print(f"created {FILENAME}")
import sys
lines = open(sys.argv[1]).read().strip().split("\n")

def read_input(lines):
	rules = {}
	template = lines[0]
	for line in lines[2:]:
		rule = line.split("->")
		rules[rule[0].strip()] = rule[1].strip()
	return template, rules

def add_item(i, dic, amount=1):
	if i in dic:
		dic[i] += amount
	else:
		dic[i] = amount

def get_pairs(template):
	pairs = {}
	for i, t in enumerate(template[:-1]):
		add_item(t + template[i+1], pairs)
	return pairs

template, rules = read_input(lines)
last_char = template[-1]
pairs = get_pairs(template)

def step(pairs, rules):
	new_pairs = {}
	for p in pairs:
		insert = rules[p]
		add_item(p[0]+insert, new_pairs, pairs[p])
		add_item(insert + p[1], new_pairs, pairs[p])
	return new_pairs

STEPS = int(sys.argv[2])
for s in range(1,STEPS+1):
	pairs = step(pairs, rules)
	print(f"{s}")
print(pairs)
singles = {}
for p in pairs:
	add_item(p[0], singles, pairs[p])
singles[last_char] += 1
print(singles)
print(max(singles.values()) - min(singles.values()))
import sys
def read_input(f):
	dots = []
	instructions = []
	lines = open(f).read().strip().split("\n")
	read_dots = True
	for line in lines:
		if line == "":
			read_dots = False
		else:
			if read_dots:
				dots.append(list(map(int, line.split(","))))
			else:
				dim, pos = line.split("=")
				instructions.append(
					(dim[-1], int(pos))
				)
	return dots, instructions

dots, instructions = read_input(sys.argv[1])

def fold_dot(point, line):
	if point > line:
		return line - (point-line)
	else:
		return point

def fold_dots(dots, instruction):
	dim, pos = instruction
	print(f"folding dots across {dim}={pos}")
	if dim == "y":
		for dot in dots:
			dot[1] = fold_dot(dot[1], pos)
	elif dim == "x":
		for dot in dots:
			dot[0] = fold_dot(dot[0], pos)
	else:
		pass

def unique_dots(dots):
	dots = set([tuple(d) for d in dots])
	return [list(d) for d in dots]

for i in instructions:
	fold_dots(dots, i)

import matplotlib.pyplot as plt
def print_dots(dots):
	scale_dot = [[0,30]]
	plt.scatter(*zip(*dots+scale_dot))
	plt.gca().invert_yaxis()
	plt.savefig("out.png")

u = unique_dots(dots)
print_dots(u)
print(len(u))
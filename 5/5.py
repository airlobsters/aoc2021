inputs = open("input.txt").read().strip().split("\n")
inputs = [x.split() for x in inputs]
print(inputs[0])
lines = [(x[0].split(","), x[2].split(",")) for x in inputs]
lines = [[[int(c[0]), int(c[1])] for c in line] for line in lines]
WIDTH, HEIGHT = 1000,1000
faults = [[0 for x in range(WIDTH)]for x in range(HEIGHT)]
print(lines[0])
print(faults[0][0])

def point_list(start, end):
	if end >= start:
		return [x for x in range(start, end+1)]
	else:
		return [x for x in range(start, end-1, -1)]

assert point_list(10,10) ==[10]
assert point_list(10,12) ==[10,11,12]
assert point_list(12,10) ==[12,11,10]

def coordinates(line):
	start, end = line
	xs = point_list(start[0],end[0])
	ys = point_list(start[1],end[1])
	if len(xs) == 1 or len(ys) == 1:
		return [(x,y) for x in xs for y in ys]
	else:
		return [(xs[i], ys[i])for i in range(len(xs))]

#print(coordinates(([1,1],[1,3])))
#print(coordinates(([1,1],[3,1])))
#print(coordinates(([1,1],[3,3])))
#print(coordinates(([4,3],[2,1])))
#print(coordinates(([4,3],[2,1])))
#print(coordinates(([2,1],[4,3])))
#print(coordinates(([4,1],[2,3])))

for line in lines:
	for c in coordinates(line):
		faults[c[1]][c[0]] +=1

print(sum([len([y for y in row if y>1]) for row in faults]))
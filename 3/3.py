inputs = open("input.txt").read().strip().split("\n")

def mode(l):
    return max(set(l), key = l.count)

def inverse(s):
    return ''.join([str(1 - int(x)) for x in s])

def gamma_rate(l):
    return ''.join([mode([x[i] for x in inputs]) for i in range(len(inputs[0]))])

def epsilon_rate(l):
    return inverse(gamma_rate(l))

print(f"{inverse('11') = }")
print(f"{inverse('00') = }")
print(f"{gamma_rate(inputs) = }")
print(f"{epsilon_rate(inputs) = }")

print(f"{int(gamma_rate(inputs),2) * int(epsilon_rate(inputs),2)}")

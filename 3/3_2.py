inputs = open("input.txt").read().strip().split("\n")

def mode(l):
    return str(int(l.count('1') >= len(l) / 2))

def most_common(l, i):
    return mode([x[i] for x in l])

def oxygen_generator_rating(l, i = 0):
    if len(l) == 1 or i == len(l[0]):
        return l
    l = [x for x in l if x[i] == most_common(l, i)]
    return oxygen_generator_rating(l, i + 1)

def CO2_scrubber_rating(l, i = 0):
    if len(l) == 1 or i == len(l[0]):
        return l
    l = [x for x in l if x[i] != most_common(l, i)]
    return CO2_scrubber_rating(l, i + 1)

print(f"{oxygen_generator_rating(inputs) = }")
print(f"{CO2_scrubber_rating(inputs) = }")

print(int(oxygen_generator_rating(inputs)[0],2) * int(CO2_scrubber_rating(inputs)[0],2))
from functools import reduce

inputs = open("input.txt").read().strip().split("\n")
w = len(inputs[0])
inputs = [int(x, 2) for x in inputs]

def mode(l):
    return max(set(l), key = l.count)

def inverse(n):
    return (~n & (2**w-1))

def gamma_rate(l):
    bitlist = [mode([x >> i-1 & 1 for x in inputs]) for i in range(w,0,-1)]
    return reduce(lambda a,b: (a << 1) | b, bitlist, 0)

def epsilon_rate(l):
    return inverse(gamma_rate(l))

print(f"{gamma_rate(inputs) = }")
print(f"{epsilon_rate(inputs) = }")

print(f"{gamma_rate(inputs) * epsilon_rate(inputs) =}")

# matrix solution
import numpy as np
m = open("input_test.txt").read().strip().split("\n")
m = np.array([[col for col in row] for row in m])
print(m)
# calculate mode for each row
#convert to int
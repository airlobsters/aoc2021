import sys
lines = open(sys.argv[1]).read().split("\n")

class Cave():
	def __init__(self, name):
		self.name = name
		self.connections = []
	def __str__(self):
		return self.name

def read_input(lines):
	caves = {}
	for line in lines:
		a, b = line.split("-")
		if not a in caves:
			ca = Cave(a)
			caves[a] = ca
		else:
			ca = caves[a]
		if not b in caves:
			cb = Cave(b)
			caves[b] = cb
		else:
			cb = caves[b]
		ca.connections.append(cb)
		cb.connections.append(ca)
	return caves

def find_paths(caves, path, visited):
	if path[-1] == "end":
		return [path]
	else:
		unvisited_connections = [c.name for c in caves[path[-1]].connections if not c.name in visited]
		temp_list = []
		for c in unvisited_connections:
			temp_list += find_paths(
			caves, 
			path + [c], 
			visited + [path[-1]] if path[-1].islower() else visited)
		return temp_list

caves = read_input(lines)
paths = find_paths(caves, ["start"], [])
print(paths)
print(len(paths))
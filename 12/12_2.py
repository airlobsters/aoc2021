import sys
lines = open(sys.argv[1]).read().split("\n")

class Cave():
	def __init__(self, name):
		self.name = name
		self.connections = []
	def __str__(self):
		return self.name

def read_input(lines):
	caves = {}
	for line in lines:
		a, b = line.split("-")
		if not a in caves:
			ca = Cave(a)
			caves[a] = ca
		else:
			ca = caves[a]
		if not b in caves:
			cb = Cave(b)
			caves[b] = cb
		else:
			cb = caves[b]
		ca.connections.append(cb)
		cb.connections.append(ca)
	return caves

import collections
def max_occ(l):
	l = [a for a in l if a.islower()]
	return max(collections.Counter(l).values())

def get_available_connections(c, visited):
	cons = []
	visited = visited + [c.name]
	for av_con in [c.name for c in c.connections]:
		if av_con.isupper():
			cons.append(av_con)
		elif av_con not in visited:
			cons.append(av_con)
		elif (visited.count(av_con) == 1 and
				max_occ(visited) == 1 and 
				av_con not in ["start", "end"]):
			cons.append(av_con)
	return cons

def find_paths(caves, path, visited):
	if path[-1] == "end":
		return [path]
	else:
		unvisited_connections = get_available_connections(caves[path[-1]], visited)
		temp_list = []
		for c in unvisited_connections:
			temp_list += find_paths(
			caves, 
			path + [c], 
			visited + [path[-1]] if path[-1].islower() else visited)
		return temp_list

caves = read_input(lines)
paths = find_paths(caves, ["start"], [])
for p in paths:
	print(",".join(p))
print(len(paths))


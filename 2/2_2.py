inputs = open("input.txt").read().strip().split("\n")

horizontal = 0
depth = 0
aim = 0

for i in inputs:
    direction, amount = i.split()
    amount = int(amount)
    if direction == "forward":
        horizontal += amount
        depth += aim*amount
    elif direction == "up":
        aim -= amount
    elif direction == "down":
        aim += amount
    else:
        raise RuntimeError()

print(horizontal*depth)


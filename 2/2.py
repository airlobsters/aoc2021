inputs = open("input.txt").read().strip().split("\n")

horizontal = 0
depth = 0

for i in inputs:
    direction, amount = i.split()
    amount = int(amount)
    if direction == "forward":
        horizontal += amount
    elif direction == "up":
        depth -= amount
    elif direction == "down":
        depth += amount
    else:
        raise RuntimeError()

print(horizontal*depth)


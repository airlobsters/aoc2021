import sys
lines = open(sys.argv[1]).read().strip().split("\n")
height_map = [[int(c) for c in line] for line in lines]

def adjacent_coords(coord, m):
	a = []
	r, c = coord
	if r != 0:
		a.append((r-1,c))
	if c != 0:
		a.append((r,c-1))
	if r != len(m) - 1:
		a.append((r+1,c))
	if c != len(m[0]) - 1:
		a.append((r,c+1))
	return a

def find_low_points(m):
	low_points = []
	for r, row in enumerate(m):
		for c, col in enumerate(row): 
			if all([m[ar][ac] > col for ar, ac in adjacent_coords((r,c), m)]):
				low_points.append((r,c))
	return low_points

def risk_level(coord, m):
	r,c = coord
	return m[r][c] + 1	

lp = find_low_points(height_map)
total = 0
for p in lp:
	total += risk_level(p, height_map)
print(f"answer 1 = {total}")

def height(coord, m):
	return m[coord[0]][coord[1]]

def extend_basin(basin, m):
	extended = False
	for coord in basin:
		extension = [a for a in adjacent_coords(coord, m)
			if height(a, m) != 9 and
			a not in basin and
			height(a, m) > height(coord, m)
		]
		if len(extension) != 0:
			extended = True
			basin.extend(extension)
	if extended:
		return extend_basin(basin, m)
	return basin


basins = []
for p in lp:
	b = [p] # current basin
	b = extend_basin(b, height_map)
	basins.append(b)
sizes = list(map(len, basins))
sizes.sort(reverse=True)

from operator import mul
from functools import reduce
answer_2 = reduce(mul, sizes[:3], 1)
print(f"{answer_2=}")

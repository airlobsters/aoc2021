import sys
l = open("input.txt").read().strip().split(",")
num_days = int(sys.argv[1])
l = [int(x) for x in l]
t = [l.count(i) for i in range(9)]
for d in range(num_days):
	print(t)
	t.append(0)
	new_fish = t.pop(0)
	t[6] += new_fish
	t[8] = new_fish

print(sum(t))

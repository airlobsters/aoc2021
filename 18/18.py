class SnailNum():
	def __init__(self, input, parent=None):
		self.parent = parent
		self.value = None
		self.l = None
		self.r = None
		if isinstance(input, SnailNum):
			self.l = input.l
			self.r = input.r
		elif isinstance(input, list):
			if isinstance(input[0], list):
				self.l = SnailNum(input[0], parent=self)
			else:
				self.l = input[0]
			if isinstance(input[1], list):
				self.r = SnailNum(input[1], parent=self)
			else:
				self.r = input[1]
		# else:
		# 	self.value = l
	def __eq__(self, other):
		return str(self) == str(other)
	def __str__(self):
		if self.l:
			return f"[{self.l},{self.r}]"
		else:
			return str(self.value)
	def add(self, s2):
		return SnailNum([self,s2])
	def first_left_value(self, previous=None):
		if isinstance(self.r, SnailNum) and isinstance(self.l, int):
			return self
		if not self.l == previous and isinstance(self.l, SnailNum):
			if self.l.first_left_value():
				return self.l.first_left_value()
		elif isinstance(self.r, SnailNum):
			if self.r.first_left_value():
				return self.r.first_left_value()
		if self.parent:
			return self.parent.first_left_value(self)
	def first_right_value(self, previous = None):
		if isinstance(self.l, SnailNum) and isinstance(self.r, int):
			return self
		if not self.r == previous and isinstance(self.r, SnailNum):
			if self.r.first_right_value():
				return self.r.first_right_value()
		elif isinstance(self.l, SnailNum):
			if self.l.first_right_value():
				return self.l.first_right_value()
		if self.parent:
			return self.parent.first_right_value(self)
	def explode(self):
		print(f"exploding {self}, parent = {self.parent}")
		flv = self.first_left_value()
		if flv:
			if isinstance(flv.r,int):
				flv.r += self.l
			else:
				flv.l += self.l
		frv = self.first_right_value()
		if frv:
			if isinstance(frv.l,int):
				frv.l += self.r
			else:
				frv.r += self.r
		if self.parent.l == self:
			self.parent.l = 0
		else:
			self.parent.r = 0
		
	def handle_explosions(self, d=0):
		if d >= 4 and self.l and isinstance(self.l, int):
			self.explode()
			return True
		if d<4
			if isinstance(self.l, SnailNum):
				return self.l.handle_explosions(d+1)
			if isinstance(self.r, SnailNum):
				return self.r.handle_explosions(d+1)
			else:
				return True
		return False
	def handle_splits(self):
		print("handle_splits called")
	def reduce(self):
		self.handle_explosions()
		self.handle_splits()

assert SnailNum([1,1]) == SnailNum([1,1])
assert SnailNum(1) == SnailNum(1)
assert not SnailNum([1,1]) == SnailNum([1,0])
assert not SnailNum([1,1]) == SnailNum(0)

def test_reduce(initial, result):
	s = SnailNum(initial)
	s.reduce()
	try:
		assert s == SnailNum(result)
	except:
		print(f"reducing {initial}\n",
		f"did not result in {result}\n",
		f"instead got {s}")
		raise

assert SnailNum([1,[9,8]]).r.first_left_value().l == 1
test_reduce([[[[[9,8],1],2],3],4], [[[[0,9],2],3],4])
test_reduce([7,[6,[5,[4,[3,2]]]]], [7,[6,[5,[7,0]]]])
test_reduce([[6,[5,[4,[3,2]]]],1], [[6,[5,[7,0]]],3])
test_reduce([[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]], [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]])


print("tests succesful")



import sys
import queue
filename = sys.argv[1] if len(sys.argv) > 1 else "input_test.txt"
lines = open(filename).read().strip().split("\n")

class Node():
	def __init__(self, id, risk, coords):
		self.parent = None
		self.distance = float("inf")
		self.id = id
		self.risk = risk
		self.coords = coords
		self.adjacent_nodes = []
		self.fa = float("inf")
		self.visited = False
	def __eq__(self, other):
		return self.id == other.id
	def __lt__(self, other):
		return self.fa < other.fa
	def __str__(self):
		return str(f"Node({self.id}, {self.risk}, {self.coords}, {self.distance})")

def adjacent_coords(c1):
	l = []
	check_coords = [(-1,0), (0,-1),(1,0), (0,1)]
	for c2 in check_coords:
		x = c1[0] + c2[0]
		y = c1[1] + c2[1]
		if x >= 0 and x < XSIZE and y >= 0 and y < YSIZE:
			l.append((x,y))
	return l

def ro9(i):
	return ((i-1) % 9) + 1

XSIZE = len(lines[0])
YSIZE = len(lines)
nodes = {}
id = 0
duplicate_x = int(sys.argv[2]) if len(sys.argv) > 2 else 1
duplicate_y = duplicate_x
for y in range(duplicate_y):
	for x in range(duplicate_x):
		for r, row in enumerate(lines):
			for c, col in enumerate(row):
				risk = ro9(int(col) + x + y)
				n = Node(id, risk, (r+y*YSIZE,c+x*XSIZE))
				nodes[(r+y*YSIZE,c+x*XSIZE)] = n
				id += 1
print(f"created {len(nodes)} nodes")
XSIZE = len(lines[0]) * duplicate_x
YSIZE = len(lines) * duplicate_y
for n in nodes:
	nodes[n].adjacent_nodes = [nodes[a] for a in adjacent_coords(n)]

def print_nodes(nodes, path=[]):
	show_path= not len(path) == 0
	p = [[0 for _ in range(XSIZE)]for _ in range(YSIZE)]
	for n in nodes:
		p[n[0]][n[1]] = nodes[n].risk
		if show_path and nodes[n].id not in path:
			p[n[0]][n[1]] = " "
	for r in p:
		for c in r:
			print(c, end="")
		print()

print_nodes(nodes)

def manhattan(a, b):
	return sum(abs(val1-val2) for val1, val2 in zip(a,b))

def h(start, end):
	return manhattan(start, end) - 0.1

def a_star(start, goal):
	q = queue.PriorityQueue()
	start.distance = 0
	q.put(start)

	while q.qsize():
		cur_node = q.get()
		if cur_node == goal:
			return cur_node
		for a in cur_node.adjacent_nodes:
			if not a.visited and cur_node.distance + a.risk < a.distance:
				a.distance = cur_node.distance + a.risk
				a.fa = a.distance + h(a.coords, goal.coords)
				a.parent = cur_node
				q.put(a)
		cur_node.visited = True

n = a_star(nodes[(0,0)], nodes[(XSIZE-1, YSIZE-1)])
final = n
path = [0]
while n.parent:
	path.append(n.id)
	n = n.parent
print_nodes(nodes, path)
print(f"{final.distance=}")
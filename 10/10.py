import sys
lines = open(sys.argv[1]).read().strip().split("\n")
m = {
    "{":"}",
    "(":")",
    "[":"]",
    "<":">",
}
s = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}
a = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4,
}

def autocomplete_score(l):
    l.reverse()
    score = 0
    for c in l:
        score *= 5
        score += a[m[c]]
    print(f"computing score for {l}: {score}")
    return score



c_open = "({[<"
score = 0
complete_scores = []
for line in lines:
    q = []
    for char in line:
        if char in m: #opening char
            q.append(char)
        elif char in m.values(): #closing char
            expected = m[q.pop()]
            if expected != char:
                print(f"{line} Expected: {expected}, found: {char}")
                score += s[char]
                q = []
                break
    if q:
        complete_scores.append(autocomplete_score(q))
print(f"answer 1 = {score}")
complete_scores.sort()
print(complete_scores)
print("answer 2 =",complete_scores[len(complete_scores)//2])
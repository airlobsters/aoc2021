import sys
import numpy as np
import math

def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis / math.sqrt(np.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])

def rotate_coord(c, axis, amount=0):
	#the rotation angle is 0.5 pi is 90degrees times the amount of times to rotate
	theta = 0.5 * math.pi * amount 
	r = np.dot(rotation_matrix(axis, theta), c)
	return np.rint(r).astype(int)

# scanner1= list(map(np.asarray, [[0,2],[4,1],[3,3]]))
# scanner2 = list(map(np.asarray, [[-1,-1],[-5,0],[-2,1]]))
# print(scanner1, scanner2)
MIN_MATCHES = 12

def count_match(ob1, ob2, d):
	"""counts number of matching coordinates in observations ob1 and ob2
	given a difference d"""
	matches =0
	for c1 in ob1:
		for c2 in ob2:
			matches += all(c1-d == c2)
	return matches

def rotate_scanner(rotation, scanner):
	"""a rotation is a tuple indicating how many times 
	a scanner should be rotated around that axis"""
	rotated_scanner = [obs for obs in scanner]
	axes = [[1,0,0],[0,1,0],[0,0,1]]
	for axis_index, amount in enumerate(rotation):
		axis = axes[axis_index]
		for o, obs in enumerate(rotated_scanner):
			rotated_scanner[o] = rotate_coord(obs, axis, amount)
	return rotated_scanner

def find_diff(scanner1, scanner2):
	for coord1 in scanner1:
		# optimize: if this is done len(scanner1) - MIN_MATCHES times, you can stop
		for rotation in rotations:
			rotated_scanner = rotate_scanner(rotation, scanner2)
			for coord2 in rotated_scanner:
				diff = coord1 - coord2
				if count_match(scanner1, rotated_scanner, diff) >= MIN_MATCHES:
					return diff, rotation
	return None, None

#90 degrees means max of 4 rotations around all axes
"""for the number of unique rotations, consider a dice:
	the first axis x you can rotate 0-3 times to get 4 sides
	the second axis y you can rotate 1 and -1 more times to get the other two sides
	each side has 4 rotations around the remaining z axis
"""
rotations = [(x, 0) for x in range(4)]
rotations += [(0, y) for y in [-1, 1]]
rotations = [(x,y,z) for x,y in rotations for z in range(4)]

filename= sys.argv[1] if len(sys.argv) > 1 else "19/input.txt"
def read_input(filename):
	scanners = []
	i = -1
	lines = open(filename).read().split("\n")
	for line in lines:
		if "scanner" in line:
			scanners.append([])
			i += 1
		elif line != "":
			scanners[i].append(
				np.asarray(list(map(int, line.split(","))))
			)
	return scanners

def print_scanner(scanner):
	print("---------------")
	for obs in scanner:
		print("".join(str(obs)))

scanners = read_input(filename)
# for i, rotation in enumerate(rotations):
# 	rotated_scanner = rotate_scanner(rotation, scanners[0])
# 	print(i)
# 	print_scanner(rotated_scanner)


pairs = [[0, 27],
[1, 6],
[1, 21],
[1, 31],
[1, 36],
[2, 33],
[3, 4],
[3, 25],
[4, 10],
[5, 20],
[5, 32],
[6, 34],
[6, 37],
[7, 13],
[7, 23],
[8, 23],
[8, 24],
[8, 26],
[9, 20],
[9, 23],
[9, 25],
[9, 26],
[10, 19],
[10, 25],
[11, 15],
[12, 26],
[12, 35],
[13, 22],
[14, 16],
[14, 33],
[15, 17],
[15, 20],
[15, 25],
[17, 18],
[17, 31],
[18, 20],
[18, 26],
[18, 30],
[22, 32],
[28, 27],
[30, 28],
[29, 36],
[31, 37],
[32, 33],
[34, 36]]

def order_pairs(current):
	if current == []:
		return []
	matchlist = [c[1] for c in current]
	fromlist = [c[0] for c in current]
	found_list = ([[a,b] for a,b in pairs if a in matchlist and b not in fromlist] +
		[[b,a] for a,b in pairs if b in matchlist and a not in fromlist])
	return current + order_pairs(found_list)
ordered_pairs = order_pairs([pairs[0]])
print(ordered_pairs)
print(len(ordered_pairs), len(pairs))
found_positions = [None for _ in range(len(scanners))]
found_rotations = [None for _ in range(len(scanners))]
found_positions[0] = np.asarray([0,0,0])
found_rotations[0] = [0,0,0]
beacon_map = []
beacon_map += scanners[0]
def process_pair(pair):
	i,j = pair
	print(f"finding diff between scanner {i} and {j}")
	relative_position, rotation = find_diff(scanners[i], scanners[j])
	#if a match has been found
	if rotation != None:
		print(f"found {relative_position, rotation}")
		if not found_positions[i] is None:
			print(f"found {j} based on {i}")
			scanners[j] = rotate_scanner(rotation, scanners[j])
		else:
			print(f"found {i} based on {j}")
			scanners[i] = rotate_scanner(rotation, scanners[i])
		
	return relative_position, rotation

for pnum, pair in enumerate(ordered_pairs):
	print(f"{pnum}/{len(pairs)}")
	i,j = pair
	relative_position, rotation = process_pair(pair)
	if not rotation is None:
		if not found_positions[i] is None:
			found_positions[j] = found_positions[i] + np.asarray(relative_position)
			for coord in scanners[j]:
				beacon_map += [found_positions[j] + coord]
			found_rotations[j] = rotation
		elif not found_positions[j] is None:
			found_positions[i] = found_positions[j] - relative_position
			found_rotations[i] = f"inverse{rotation}"
print(f"{len(beacon_map)}")
beacon_map = list(set([tuple(c) for c in beacon_map]))
print(f"answer 1 = {len(beacon_map)}")
def manhattan(a, b):
	return sum(abs(val1-val2) for val1, val2 in zip(a,b))
	
max_dist = 0
for p1 in found_positions:
	for p2 in found_positions:
		new_dist = manhattan(p1,p2)
		if new_dist > max_dist:
			max_dist = new_dist
print(f"answer 2 ={max_dist}")
print(found_positions)
print(found_rotations)
# d = np.asarray([1125, -168,   72])
# d = rotate_coord(d, np.asarray([1,0,0]),1)
# d = rotate_coord(d, np.asarray([0,0,1]),2)
# f4 = np.asarray([ -20,  -1133, 1061])
# f4 = f4-d
# print(f4)

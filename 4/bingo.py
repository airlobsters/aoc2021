import sys

class BingoCard():
	SIZE = 5
	def __init__(self):
		self.rows=[]
		self.marked = [[False for _ in range(BingoCard.SIZE)] for _ in range(BingoCard.SIZE)]
	
	def mark(self, number):
		for rowidx, row in enumerate(self.rows):
			for colidx, col in enumerate(row):
				self.marked[rowidx][colidx] += col == number
		return self.marked
	
	def has_won(self):
		#horizontal bingo
		for row in self.marked:
			if sum(row) == BingoCard.SIZE:
				return True
		# vertical bingo
		for i in range(BingoCard.SIZE):
			if sum([row[i] for row in self.marked]) == BingoCard.SIZE:
				return True
		return False
	
	def calculate_score(self):
		score = 0
		for rowidx, row in enumerate(self.rows):
			for colidx, col in enumerate(row):
				score += col if not self.marked[rowidx][colidx] else 0
		return score
		
def parse_line(l):
	return [int(x) for x in l.split(" ") if x!=""]

def read_cards(input):
	cards = []
	for line in input[1:]:
		if line != "":
			cards[-1].rows.append(parse_line(line))
		else:
			cards.append(BingoCard())
	return cards
			
def q1(cards):
	for number in numbers:
		for card in cards:
			card.mark(number)
			if card.has_won():
				return(f"answer 1 = {number * card.calculate_score()}")

def q2(cards):
	winning_cards = 0
	for number in numbers:
		for card in cards:
			if not card.has_won():
				card.mark(number)
				if card.has_won():
					winning_cards += 1
					if winning_cards == len(cards):
						return(f"answer 2 = {number * card.calculate_score()}")

input = open(sys.argv[1]).read().strip().split("\n")
numbers = list(map(int, input[0].split(",")))

print(q1(read_cards(input)))
print(q2(read_cards(input)))